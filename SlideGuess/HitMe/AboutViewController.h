//
//  AboutViewController.h
//  HitMe
//
//  Created by demo on 6/25/13.
//  Copyright (c) 2013 Mohit Sadhu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AboutViewController : UIViewController

- (IBAction)close;

@property (strong, nonatomic) IBOutlet UIWebView * webView;

@end
