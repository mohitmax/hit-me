//
//  NumberViewController.m
//  HitMe
//
//  Created by demo on 6/28/13.
//  Copyright (c) 2013 Mohit Sadhu. All rights reserved.
//

#import "NumberViewController.h"
#import "StartViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface NumberViewController ()

@end

@implementation NumberViewController
{
    int currentValue;
    int score;
    int round;
    bool overloadFlag;
}

@synthesize targetNumberLabel = _targetNumberLabel;
@synthesize scoreNumberLabel = _scoreNumberLabel;
@synthesize roundNumberLabel = _roundNumberLabel;
@synthesize answerText = _answerText;
@synthesize sliderNumber = _sliderNumber;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self startNewNumberGame];
    
    UIImage *thumbImageNormal = [UIImage imageNamed:@"SliderThumb-Normal"];
    [self.sliderNumber setThumbImage:thumbImageNormal forState:UIControlStateNormal];
    
    UIImage *thumbImageHighlighted = [UIImage imageNamed:@"SliderThumb-Highlighted"];
    [self.sliderNumber setThumbImage:thumbImageHighlighted forState:UIControlStateHighlighted];
    
    UIImage *trackLeftImage = [[UIImage imageNamed:@"SliderTrackLeft"] stretchableImageWithLeftCapWidth:14 topCapHeight:0];
    [self.sliderNumber setMinimumTrackImage:trackLeftImage forState:UIControlStateNormal];
    
    
    UIImage *trackRightImage = [[UIImage imageNamed:@"SliderTrackRight"] stretchableImageWithLeftCapWidth:14 topCapHeight:0];
    [self.sliderNumber setMaximumTrackImage:trackRightImage forState:UIControlStateNormal];
}

- (void) startNewNumberGame
{
    score = 0;
    round = 0;
    
    [self startNewNumberRound];
}

- (void) startNewNumberRound
{
    //This flag will check if the value entered is more than 100
    overloadFlag = FALSE;
    
    _answerText.text = @"";
    round += 1;
    
    currentValue = 1 + (arc4random() % 100);
    self.sliderNumber.value = currentValue;
    
    [self updateLabels];
}

- (void) updateLabels
{
    self.scoreNumberLabel.text = [NSString stringWithFormat:@"%d", score];
    self.roundNumberLabel.text = [NSString stringWithFormat:@"%d", round];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setTargetNumberLabel:nil];
    [self setScoreNumberLabel:nil];
    [self setRoundNumberLabel:nil];
    [self setSliderNumber:nil];
    [self setAnswerText:nil];
    [super viewDidUnload];
}

//this method is called when the "Hit Me" button is tapped.
- (IBAction)showNumberAlert:(UIButton *)sender
{
    int answerValue = [_answerText.text intValue];
    
    if (answerValue > 100)
    {
        overloadFlag = TRUE;
    }
    
    int difference = abs(_sliderNumber.value - answerValue);
    int bonusPoints = 0;
    NSString *messageNumber;
    
    //have to add logic to calculate teh score and rounds
    int points = 100 - difference;
    
    NSString *title;
    if (overloadFlag)
    {
        title = @"Value out of scope.";
        points = 0;
        bonusPoints = -10;
    }
    else if (answerValue == 0)
    {
        title = @"Hmm. No value. No points.";
        points = 0;
        //bonusPoints = 0;
    }
    else
    {
        if (difference == 0)
        {
            title = @"Perfect!!";
            bonusPoints = 100;
        }
        else if (difference <= 10)
        {
            title = @"So Close.";
            if(difference == 1)
            {
                title = @"Almost there.";
                bonusPoints = 50;
            }
        }
        else
            title = @"Not even close.";
    }

    if (bonusPoints != 0)
    {
         messageNumber = [NSString stringWithFormat:@"Points Scored : %d \n Slider Value: %d \n Bonus Points Scored: %d", points, currentValue, bonusPoints];
    }
    else
    {
        messageNumber = [NSString stringWithFormat:@"Points Scored: %d \n Slider Value: %d", points, currentValue];

    }

    //total score is calculated
    score = score + points + bonusPoints;
    
    UIAlertView *alertView = [[UIAlertView alloc]
                initWithTitle:title
                message:messageNumber
                delegate:self
                cancelButtonTitle:@"Ok"
                otherButtonTitles:nil, nil];
    
    [alertView show];
}

//Alert delegates to itself
- (void) alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    [self startNewNumberRound];
}

//This is to start a new game with Score = 0 and Round = 1.
- (IBAction)startNumberOver:(UIButton *)sender
{
    CATransition *transition = [CATransition animation];
    transition.type = kCATransitionFade;
    transition.duration = 1;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
    
    [self startNewNumberGame];
    //[self updateLabels];
    
    [self.view.layer addAnimation:transition forKey:nil];
}

- (IBAction)menu:(UIButton *)sender
{
    StartViewController *startController = [[StartViewController alloc]
                initWithNibName:@"StartViewController" bundle:nil];
    startController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:startController animated:YES completion:nil];
}

-(void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [_answerText resignFirstResponder];
}

- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(toInterfaceOrientation);
}

@end
