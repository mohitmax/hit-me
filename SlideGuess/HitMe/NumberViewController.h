//
//  NumberViewController.h
//  HitMe
//
//  Created by demo on 6/28/13.
//  Copyright (c) 2013 Mohit Sadhu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NumberViewController : UIViewController

@property (strong, nonatomic) IBOutlet UILabel *targetNumberLabel;
@property (strong, nonatomic) IBOutlet UILabel *scoreNumberLabel;
@property (strong, nonatomic) IBOutlet UILabel *roundNumberLabel;
@property (strong, nonatomic) IBOutlet UISlider *sliderNumber;
@property (strong, nonatomic) IBOutlet UITextField *answerText;

- (IBAction)showNumberAlert:(UIButton *)sender;
- (IBAction)startNumberOver:(UIButton *)sender;
- (IBAction)menu:(UIButton *)sender;


@end
