//
//  main.m
//  HitMe
//
//  Created by demo on 6/20/13.
//  Copyright (c) 2013 Mohit Sadhu. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "HitMeAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([HitMeAppDelegate class]));
    }
}
