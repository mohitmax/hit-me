//
//  StartViewController.m
//  HitMe
//
//  Created by demo on 6/27/13.
//  Copyright (c) 2013 Mohit Sadhu. All rights reserved.
//

#import "StartViewController.h"
#import "HitMeViewController.h"
#import "AboutViewController.h"
#import "NumberViewController.h"

@interface StartViewController ()

@end

@implementation StartViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//start the game in Slider Mode.
- (IBAction)startGame:(UIButton *)sender
{
    HitMeViewController *controller = [[HitMeViewController alloc]
            initWithNibName:@"HitMeViewController"
            bundle:nil];
    controller.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:controller animated:YES completion:nil];
}

//To show Info about this application via a HTML file - web view
- (IBAction)showInfo:(UIButton *)sender
{
    AboutViewController *controller = [[AboutViewController alloc]
            initWithNibName:@"AboutViewController"
            bundle:nil];
    controller.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:controller animated:YES completion:nil];
}

- (IBAction)startNumberMode:(UIButton *)sender
{
    NumberViewController *numberGameController = [[NumberViewController alloc] initWithNibName:@"NumberViewController" bundle:nil];
    numberGameController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:numberGameController animated:YES completion:nil];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(toInterfaceOrientation);
}

@end
