//
//  StartViewController.h
//  HitMe
//
//  Created by demo on 6/27/13.
//  Copyright (c) 2013 Mohit Sadhu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StartViewController : UIViewController

- (IBAction)startGame:(UIButton *)sender;
- (IBAction)startNumberMode:(UIButton *)sender;
- (IBAction)showInfo:(UIButton *)sender;

@end
