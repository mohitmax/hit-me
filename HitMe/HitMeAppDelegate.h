//
//  HitMeAppDelegate.h
//  HitMe
//
//  Created by demo on 6/20/13.
//  Copyright (c) 2013 Mohit Sadhu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StartViewController.h"

@class HitMeViewController;

@interface HitMeAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) HitMeViewController *viewController;
@property (strong, nonatomic) StartViewController *startGameViewController;

@end
