//
//  HitMeViewController.m
//  HitMe
//
//  Created by demo on 6/20/13.
//  Copyright (c) 2013 Mohit Sadhu. All rights reserved.
//

#import "HitMeViewController.h"
#import "StartViewController.h"
#import <QuartzCore/QuartzCore.h>


@interface HitMeViewController ()

@end

@implementation HitMeViewController
{
    int currentValue;
    int currentValueAnotherSlider;
    int targetValue;
    int score;
    int round;
}

@synthesize slider = _slider;
@synthesize anotherSlider = _anotherSlider;
@synthesize targetLabel = _targetLabel;
@synthesize scoreLabel = _scoreLabel;
@synthesize roundLabel = _roundLabel;

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self startNewGame];
    
    UIImage *thumbImageNormal = [UIImage imageNamed:@"SliderThumb-Normal"];
    [self.slider setThumbImage:thumbImageNormal forState:UIControlStateNormal];
    [self.anotherSlider setThumbImage:thumbImageNormal forState:UIControlStateNormal];
    
    UIImage *thumbImageHighlighted = [UIImage imageNamed:@"SliderThumb-Highlighted"];
    [self.slider setThumbImage:thumbImageHighlighted forState:UIControlStateHighlighted];
    [self.anotherSlider setThumbImage:thumbImageHighlighted forState:UIControlStateHighlighted];
    
    UIImage *trackLeftImage = [[UIImage imageNamed:@"SliderTrackLeft"] stretchableImageWithLeftCapWidth:14 topCapHeight:0];
    [self.slider setMinimumTrackImage:trackLeftImage forState:UIControlStateNormal];
    [self.anotherSlider setMinimumTrackImage:trackLeftImage forState:UIControlStateNormal];
    
    UIImage *trackRightImage = [[UIImage imageNamed:@"SliderTrackRight"] stretchableImageWithLeftCapWidth:14 topCapHeight:0];
    [self.slider setMaximumTrackImage:trackRightImage forState:UIControlStateNormal];
    [self.anotherSlider setMaximumTrackImage:trackRightImage forState:UIControlStateNormal];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)showAlert:(UIButton *)sender
{
    int bonusPoints = 0;
    NSString *message;
    
    int difference = abs((currentValue + currentValueAnotherSlider) - targetValue);
    int points = (200 - difference)/2;
    
    NSString *title;
    if (difference == 0)
    {
        title = @"Perfect!!";
        bonusPoints = 100;
    }
    else if (difference <= 10)
    {
        title = @"So Close.";
        if(difference == 1)
        {
            title = @"Almost there.";
            bonusPoints = 50;
        }
    }
    else
        title = @"Not even close.";
    
    if(bonusPoints != 0)
    {
        message = [NSString stringWithFormat:@"Points Scored: %d \nSlider Value: %d \n Bonus Points Scored: %d", points, (currentValue + currentValueAnotherSlider), bonusPoints];
    }
    else
    {
        message = [NSString stringWithFormat:@"Points Scored: %d \nSlider Value %d", points, (currentValue + currentValueAnotherSlider)];
        
    }
    
    //The total score is calculated here
    score = score + points + bonusPoints;
    
    UIAlertView *alertView = [[UIAlertView alloc]
        initWithTitle:title
        message:message
        delegate:self
        cancelButtonTitle:@"Ok"
        otherButtonTitles:nil, nil];
    
    [alertView show];
}

- (void) alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    [self startNewRound];
}

- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(toInterfaceOrientation);
}

- (void)viewDidUnload {
    [self setTargetLabel:nil];
    [self setScoreLabel:nil];
    [self setRoundLabel:nil];
    [self setAnotherSlider:nil];
    [super viewDidUnload];
}

- (IBAction)sliderMoved:(UISlider *)sender
{
    currentValue = lroundf(sender.value);
}

- (IBAction)anotherSliderMoved:(UISlider *)sender
{
    currentValueAnotherSlider = lroundf(sender.value);
}

- (IBAction)startOver:(UIButton *)sender
{
    CATransition *transition = [CATransition animation];
    transition.type = kCATransitionFade;
    transition.duration = 1;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
    
    [self startNewGame];
    [self updateLabels];
    
    [self.view.layer addAnimation:transition forKey:nil];
}

- (void) updateLabels
{
    self.targetLabel.text = [NSString stringWithFormat:@"%d", targetValue];
    self.scoreLabel.text = [NSString stringWithFormat:@"%d", score];
    self.roundLabel.text = [NSString stringWithFormat:@"%d", round];
}

- (void) startNewRound
{
    round += 1;
    
    targetValue = 1 + (arc4random() % 200);
    if (targetValue == 1)
        targetValue = 2;
    
    currentValue = 1;
    currentValueAnotherSlider = 1;
    self.slider.value = currentValue;
    self.anotherSlider.value = currentValueAnotherSlider;
    
    [self updateLabels];
}

- (void) startNewGame
{
    score = 0;
    round = 0;
    [self startNewRound];
}

- (IBAction)menu:(UIButton *)sender
{
    StartViewController *startController = [[StartViewController alloc]
                initWithNibName:@"StartViewController" bundle:nil];
    startController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:startController animated:YES completion:nil];
}
@end
