//
//  HitMeViewController.h
//  HitMe
//
//  Created by demo on 6/20/13.
//  Copyright (c) 2013 Mohit Sadhu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HitMeViewController : UIViewController <UIAlertViewDelegate>

@property (strong, nonatomic) IBOutlet UISlider *slider;
@property (strong, nonatomic) IBOutlet UISlider *anotherSlider;
@property (strong, nonatomic) IBOutlet UILabel *targetLabel;
@property (strong, nonatomic) IBOutlet UILabel *scoreLabel;
@property (strong, nonatomic) IBOutlet UILabel *roundLabel;

- (IBAction)showAlert:(UIButton *)sender;
- (IBAction)sliderMoved:(UISlider *)sender;
- (IBAction)anotherSliderMoved:(UISlider *)sender;
- (IBAction)startOver:(UIButton *)sender;
//- (IBAction)showInfo:(UIButton *)sender;
- (IBAction)menu:(UIButton *)sender;


@end
